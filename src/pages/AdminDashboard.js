import {useEffect, useState, useContext} from 'react';
import {Container, Button, Table} from 'react-bootstrap';
import {Link} from 'react-router-dom';
import AdminDashboardTable from '../components/AdminDashboardTable.js';
import UserContext from '../UserContext.js';
import AddProduct from '../components/AddProduct.js';

function AdminDashboard(){

	const [products, setProducts] = useState([]);
	const {user} = useContext(UserContext);
	const [modalShow, setModalShow] = useState(false);

	useEffect(() => {
		fetch(`${process.env.REACT_APP_TINDERELLA_URL}/product/allProducts`, {
        headers:{
            Authorization: `Bearer ${localStorage.getItem('token')}`
        }
    })
		.then(result => result.json())
		.then(data => {
			setProducts(data.map(product => {
				return(
					<AdminDashboardTable key = {product._id} prop = {product}/>
				)
			}))
		})
	}, []);

	return(
		/*user ?
	    <Navigate to ="*"/>
	    :*/
		<Container className="text-center mt-5">
			<h1 className="mb-3">Admin Dashboard</h1>
			<div className="mb-5">
				<Button className="me-1" onClick={() => setModalShow(true)}>Add Product</Button>
				<AddProduct
			        show={modalShow}
			        onHide={() => setModalShow(false)}
				/>
				<Button as={Link} to={"/all-orders"}>View Orders</Button>
			</div>
			<Table striped bordered hover>
				<thead>
				    <tr>
				        <th>Name</th>
				        <th>Description</th>
				        <th>Price</th>
				        <th>Availability</th>
				        <th>Actions</th>
				    </tr>
				</thead>
				<tbody>
				    {products}
				</tbody>
			</Table>
    	</Container>
	)
};

export default AdminDashboard;