import UserContext from "../UserContext.js";
import {useState, useEffect, useContext} from 'react';
import MyOrdersTable from "../components/MyOrdersTable.js";
import {Container, Table} from 'react-bootstrap';
import {Navigate} from 'react-router-dom';

function MyOrders(){

	const [orders, setOrders] = useState([]);
	const {user} = useContext(UserContext);

	useEffect(() => {
		fetch(`${process.env.REACT_APP_TINDERELLA_URL}/order/summary/${user.id}`, {
        headers:{
            Authorization: `Bearer ${localStorage.getItem('token')}`
        }
    })
    	.then(result => result.json())
    	.then(data => {
    		setOrders(data.map(order => {
    			return(
    				<MyOrdersTable key={order._id} prop={order}/>
    			)
    		}))
    	})
	},[]);

	return(
		<Container>
			<h1 className="text-center my-5">My Orders</h1>
			<Table striped bordered hover>
				<thead>
				    <tr>
				        <th>Name</th>
				        <th>Description</th>
				        <th>Price</th>
				        <th>Quantity</th>
				        <th>Total Amount</th>
				        <th>Purchase Date</th>
				    </tr>
				</thead>
				<tbody>
				    {orders}
				</tbody>
			</Table>
		</Container>
	)
}

export default MyOrders;