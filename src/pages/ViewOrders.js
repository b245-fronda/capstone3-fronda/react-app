import {useState, useEffect} from 'react';
import {Link} from 'react-router-dom';
import {Container, Table, Button} from 'react-bootstrap';
import ViewOrdersTable from '../components/ViewOrdersTable.js';

function AllOrders(){

	const [orders, setOrders] = useState([]);

	useEffect(() => {
		fetch(`${process.env.REACT_APP_TINDERELLA_URL}/order/allOrders`, {
        headers:{
            Authorization: `Bearer ${localStorage.getItem('token')}`
        }
    })
		.then(result => result.json())
		.then(data => {
			setOrders(data.map(order => {
				return(
					<ViewOrdersTable key = {order._id} prop = {order}/>
				)
			}))
		})
	}, []);

	return(
		<Container>
			<h1 className="text-center mt-5 mb-3">AllOrders</h1>
			<div className="d-flex justify-content-center mb-5">
			<Button as={Link} to={"/adminDashboard"}>Back to Dashboard</Button>
			</div>
			<Table striped bordered hover>
				<thead>
				    <tr>
				        <th>Username</th>
				        <th>Product</th>
				        <th>Price</th>
				        <th>Quantity</th>
				        <th>Total</th>
				    </tr>
				</thead>
				<tbody>
				    {orders}
				</tbody>
			</Table>
		</Container>
	)
}

export default AllOrders;