import {useState, useEffect, useContext} from 'react';
import {Container, Button, Form, Card} from 'react-bootstrap';
import {Link, Navigate, useNavigate} from 'react-router-dom';
import UserContext from '../../UserContext.js';
import Swal from 'sweetalert2';
import './Register.css';

function Register() {

  const [username, setUsername] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");
  const navigate = useNavigate();
  const {user} = useContext(UserContext);
  const [isActive, setIsActive] = useState(false);

  useEffect(()=>{
    if(email !== "" && password !== "" && confirmPassword !== "" && password === confirmPassword){
      setIsActive(true);
    }
    else{
      setIsActive(false);
    }
  }, [email, password, confirmPassword]);

  function register(event){
    event.preventDefault();
    fetch(`${process.env.REACT_APP_TINDERELLA_URL}/user/register`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        username: username,
        email: email,
        password: password,
        confirmPassword: confirmPassword
      })
    })
    .then(result => result.json())
    .then(data => {
      console.log(data);
      if(data === false){
        Swal.fire({
          title: "Registration failed!",
          icon: "error",
          text: "Please try again!"
        })
      }else{
        Swal.fire({
          title: "Registration successful!",
          icon: "success",
          text: "You may now login!"
        })
        navigate('/login');
      }
    })
  };

  return (
    user?
    <Navigate to ="*"/>
    :
    <Container id="container-register" fluid>
      <Container>
        <Card className="col-12 col-md-8 offset-md-2 col-lg-4 offset-lg-7">
          <Form id="form-register" className="p-5" onSubmit = {event => register(event)}>
            <h3 className="text-center mb-5">Register</h3>
            <Form.Group className="mb-3" controlId="formBasicUsername">
              <Form.Label>Username:</Form.Label>
              <Form.Control
                type="string"
                placeholder="Username"
                value={username}
                onChange={event => setUsername(event.target.value)}
                required
              />
            </Form.Group>

            <Form.Group className="mb-3" controlId="formBasicEmail">
              <Form.Label>Email:</Form.Label>
              <Form.Control
                type="email"
                placeholder="name@example.com"
                value={email}
                onChange={event => setEmail(event.target.value)}
                required
              />
            </Form.Group>

            <Form.Group className="mb-3" controlId="formBasicPassword">
              <Form.Label>Password:</Form.Label>
              <Form.Control
                type="password"
                placeholder="Password"
                value={password}
                onChange={event => setPassword(event.target.value)}
                required
              />
            </Form.Group>

            <Form.Group className="mb-4" controlId="formConfirmPassword">
              <Form.Label>Confirm Password:</Form.Label>
              <Form.Control
                type="password"
                placeholder="Confirm Password"
                value={confirmPassword}
                onChange={event => setConfirmPassword(event.target.value)}
                required
              />
            </Form.Group>

            {
              isActive?
              <Button id="register-button" type="submit" className="mb-5">
                Register
              </Button>
              :
              <Button id="register-button" type="submit" className="mb-5" disabled>
                Register
              </Button>
            }

            <Form.Group className="text-center">
              <Form.Label>Already have an account? <Link to={"/login"}>Login</Link></Form.Label>
            </Form.Group>
          </Form>
        </Card>
      </Container>
    </Container>
  );
}

export default Register;