import {useEffect, useState} from 'react';
import {Container, Row} from 'react-bootstrap';
import ProductCard from '../components/ProductCard.js';

function Products() {

	const [products, setProducts] = useState([]);

	useEffect(() => {
		fetch(`${process.env.REACT_APP_TINDERELLA_URL}/product/allActiveProducts`)
		.then(result => result.json())
		.then(data => {
			setProducts(data.map(product => {
				return(
					<ProductCard key = {product._id} productProp = {product}/>
				)
			}))
		})
	}, []);

	return(
		<Container id="background" fluid>
			<h1 className="text-center pt-5 pb-4">Products Catalog</h1>
			<Row className="d-flex justify-content-center">
				{products}
			</Row>
		</Container>
	)
}

export default Products;