function ViewOrdersTable({prop}){

	const {username, name, price, quantity, totalAmount} = prop;

	return(
		<tr>
			<td>{username}</td>
			<td>{name}</td>
			<td>{price}</td>
			<td>{quantity}</td>
			<td>{totalAmount}</td>
		</tr>
	)
}

export default ViewOrdersTable;