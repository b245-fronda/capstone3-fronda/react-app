import {useState, useEffect, useContext} from 'react';
import {Container, Form, Button, Row, Col, Card} from 'react-bootstrap';
import {Link, useParams, Navigate, useNavigate} from 'react-router-dom';
import UserContext from '../UserContext.js';
import Swal from 'sweetalert2';

function OrderProduct() {

	const [userId, setUserId] = useState("");
	const [username, setUsername] = useState("");
	const [image, setImage] = useState("");
	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
	const [quantity, setQuantity] = useState(1);
	const {user} = useContext(UserContext);
	const {productId} = useParams();
	const navigate = useNavigate();

	useEffect(() => {
		fetch(`${process.env.REACT_APP_TINDERELLA_URL}/product/${productId}`)
		.then(result => result.json())
		.then(data => {
			setImage(data.image);
			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
		})
	}, [productId]);

	useEffect(() => {
		fetch(`${process.env.REACT_APP_TINDERELLA_URL}/user/details`, {
        headers:{
            Authorization: `Bearer ${localStorage.getItem('token')}`
        }
    })
		.then(result => result.json())
		.then(data => {
			setUserId(data._id);
			setUsername(data.username);
		})
	}, []);

	const orderProduct = (event) => {
	  event.preventDefault();
	  fetch(`${process.env.REACT_APP_TINDERELLA_URL}/order/${productId}`, {
	    method: 'POST',
	    headers: {
	      Authorization: `Bearer ${localStorage.getItem('token')}`,
	      'Content-Type': 'application/json'
	    },
	    body: JSON.stringify({
	      userId: userId,
	      username: username,
	      productId: productId,
	      image: image,
	      name: name,
	      description: description,
	      price: price,
	      quantity: quantity,
	      totalAmount: price*quantity
	    })
	  })
	  .then(result => result.json())
	  .then(data => {

	    if(data === false){
	      Swal.fire({
            title: "Operation failed!",
            icon: "error",
            text: "Please try again!"
          })
	    }else{
	    	Swal.fire({
	    	  title: "Order successful!",
	    	  icon: "success",
	    	  text: "Check out more of our products!"
	    	})
	      navigate('/products');
	    }
	  })
	};

	
	  
	let incNum =()=>{
	  if(quantity<10)
	  {
	    setQuantity(Number(quantity)+1);
	  }
	};
	let decNum = () => {
	  if(quantity>1)
	  {
	    setQuantity(quantity - 1);
	  }
	};
	let handleChange = (event)=>{
	  setQuantity(event.target.value);
	};
	

	return(
		user?
		<Container id="background" fluid>
			<Container className="pb-5">
				<h1 className="text-center py-5">Order Product</h1>
				<Card>
					<Container fluid>
						<Row>
							<Col className="p-3">
								<Card.Img variant="top" src={image}/>
							</Col>
							<Col className="d-flex align-items-center p-0">
								<Card.Body>
									<Card.Text as={Link} to={"/products"}>Back to Products</Card.Text>
					  				<h3 className="mb-3">Select Quantity</h3>
					  				<Form className="d-flex flex-column" onSubmit = {event => orderProduct(event)}>
					  					<Col className="d-flex col-xl-3">
			  								<button className="btn btn-outline-primary" type="button" onClick={decNum}>-</button>
				  							<input type="text" className="form-control" value={quantity} onChange={handleChange}/>
			  								<button className="btn btn-outline-primary" type="button" onClick={incNum}>+</button>
			  							</Col>
			  							<Col>
					  						<Button className="mt-4" variant="primary" type="submit">Order</Button>
					  					</Col>
					  				</Form>
								</Card.Body>
							</Col>
						</Row>
					</Container>
				</Card>
			</Container>
		</Container>
		:
		<Navigate to ="/login"/>
	)
}

export default OrderProduct;