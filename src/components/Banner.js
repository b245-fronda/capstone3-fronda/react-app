import {Container, Carousel, Button} from 'react-bootstrap';
import {Link} from 'react-router-dom';

function Banner() {
  return (
    <Container className="d-flex flex-column align-items-center" fluid>
    <div className="d-flex justify-content-center mb-4">
    <Carousel className="col-6 mt-5">
      <Carousel.Item>
        <img
          className="d-block w-100"
          src="https://res.cloudinary.com/dm2aaqucx/image/upload/v1677035241/blubel-TL5JFCvITp4-unsplash_1_qckjuv.jpg"
          alt="First slide"
        />
        <Carousel.Caption>
          <h4>Dubai/Aussie Pre-loved Mini Dress Collection</h4>
        </Carousel.Caption>
      </Carousel.Item>
      <Carousel.Item>
        <img
          className="d-block w-100"
          src="https://res.cloudinary.com/dm2aaqucx/image/upload/v1677035241/tamara-bellis-uN1m9Ca0aqo-unsplash_ypr4e9.jpg"
          alt="Second slide"
        />

        <Carousel.Caption>
          <h4>Dubai/Aussie Pre-loved Mini Dress Collection</h4>
        </Carousel.Caption>
      </Carousel.Item>
      <Carousel.Item>
        <img
          className="d-block w-100"
          src="https://res.cloudinary.com/dm2aaqucx/image/upload/v1677035241/deni-febriliyan-t_5cJWQigoA-unsplash_m8cbms.jpg"
          alt="Third slide"
        />

        <Carousel.Caption>
          <h4>Dubai/Aussie Pre-loved Mini Dress Collection</h4>
        </Carousel.Caption>
      </Carousel.Item>
      <Carousel.Item>
        <img
          className="d-block w-100"
          src="https://res.cloudinary.com/dm2aaqucx/image/upload/v1677035249/isabela-kronemberger-zdKEL1oI5uA-unsplash_1_ebjkc9.jpg"
          alt="Fourth slide"
        />

        <Carousel.Caption>
          <h4>Dubai/Aussie Pre-loved Mini Dress Collection</h4>
        </Carousel.Caption>
      </Carousel.Item>
      <Carousel.Item>
        <img
          className="d-block w-100"
          src="https://res.cloudinary.com/dm2aaqucx/image/upload/v1677035241/pexels-leeloo-thefirst-5026500_nhgmtk.jpg"
          alt="Fifth slide"
        />

        <Carousel.Caption>
          <h4>Dubai/Aussie Pre-loved Mini Dress Collection</h4>
        </Carousel.Caption>
      </Carousel.Item>
    </Carousel>
    </div>
    <div>
    <Button as={Link} to={"/products"}>Browse products</Button>
    </div>
    </Container>
  );
}

export default Banner;