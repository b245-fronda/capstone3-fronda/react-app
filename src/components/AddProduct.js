import {useState} from 'react';
import {Button, Modal, Form} from 'react-bootstrap';
import Swal from 'sweetalert2';

function AddProduct(props) {

  const [image, setImage] = useState("");
  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [price, setPrice] = useState("");

  const addProduct = (event) => {
    event.preventDefault();
    fetch(`${process.env.REACT_APP_TINDERELLA_URL}/product/addProduct`, {
      method: 'POST',
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`,
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        image: image,
        name: name,
        description: description,
        price: price
      })
    })
    .then(result => result.json())
    .then(data => {

      if(data === false){
        Swal.fire({
          title: "Operation failed!",
          icon: "error",
          text: "Please try again!"
        });
      }else{
        Swal.fire({
          title: "Success!",
          icon: "success",
          text: "Product added!"
        });
        setImage("");
        setName("");
        setDescription("");
        setPrice("");
        /*window.location.reload(true);*/
      }
    })
  };

  return (
    <Modal
      {...props}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          Add Product
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Form className="p-5" onSubmit = {event => addProduct(event)}>
          <Form.Group className="mb-3" controlId="formBasicProduct">
            <Form.Label>Product Name</Form.Label>
            <Form.Control
              type="string"
              placeholder="Product Name"
              value={name}
              onChange={event => setName(event.target.value)}
              required
            />
          </Form.Group>

          <Form.Group className="mb-3" controlId="formBasicPrice">
            <Form.Label>Price</Form.Label>
            <Form.Control
              type="string"
              placeholder="Price"
              value={price}
              onChange={event => setPrice(event.target.value)}
              required
            />
          </Form.Group>

          <Form.Group className="mb-3" controlId="formBasicDescription">
            <Form.Label>Description</Form.Label>
            <Form.Control
              as="textarea" rows={3}
              type="string"
              placeholder="Description"
              value={description}
              onChange={event => setDescription(event.target.value)}
              required
            />
          </Form.Group>

          <Form.Group className="mb-5" controlId="formBasicImage">
            <Form.Label>Image URL</Form.Label>
            <Form.Control
              type="string"
              placeholder="Image URL"
              value={image}
              onChange={event => setImage(event.target.value)}
              required
            />
          </Form.Group>

          <Button className="me-1" onClick={props.onHide}>Close</Button>
          <Button variant="primary" type="submit">
            Add Product
          </Button>
        </Form>
      </Modal.Body>
    </Modal>
  );
}

export default AddProduct;