import {Card, Button} from 'react-bootstrap';
import {Link} from 'react-router-dom';

function ProductCard({productProp}) {

  const {_id, image, name, price} = productProp;

  return (
    <Card className="col-10 col-md-5 col-lg-3 col-xl-2 m-3 p-0">
      <Card.Img variant="top" src={image} className="p-3"/>
      <Card.Body>
        <Card.Text as={Link} to={`/products/${_id}`}>Details</Card.Text>
        <Card.Title className="mb-3">{name}</Card.Title>
        <Card.Subtitle>Price:</Card.Subtitle>
        <Card.Text className="mb-3">{price}</Card.Text>
        <Button variant="primary" className="me-1" as={Link} to={`/order/${_id}`}>Order</Button>
        <Button variant="primary">Add to Cart</Button>
      </Card.Body>
    </Card>
  );
}

export default ProductCard;