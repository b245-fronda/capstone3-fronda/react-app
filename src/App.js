import './App.css';
import NavBar from './components/NavBar/NavBar.js';
import Home from './pages/Home.js';
import Register from './pages/Register/Register.js';
import Login from './pages/Login/Login.js';
import Logout from './pages/Logout.js';
import AdminDashboard from './pages/AdminDashboard.js';
import Products from './pages/Products.js';
import ProductDetails from './components/ProductDetails';
import OrderProduct from './components/OrderProduct.js';
import UpdateProduct from './components/UpdateProduct.js';
import ArchiveProduct from './components/ArchiveProduct.js';
import MyOrders from './pages/MyOrders';
import ViewOrders from './pages/ViewOrders';
import PageNotFound from './pages/PageNotFound.js';
import {useState, useEffect} from 'react';
import {UserProvider} from './UserContext.js';
import {BrowserRouter as Router, Routes, Route} from 'react-router-dom';

function App() {

  const [user, setUser] = useState(null);

  const unSetUser = () => {
    localStorage.clear();
  };

  useEffect(() => {
    fetch(`${process.env.REACT_APP_TINDERELLA_URL}/user/details`, {
        headers:{
            Authorization: `Bearer ${localStorage.getItem('token')}`
        }
    })
    .then(result => result.json())
    .then(data => {

        if(localStorage.getItem('token') !== null){
          setUser({
            id: data._id,
            isAdmin: data.isAdmin
          })
        }else{
          setUser(null);
        }
    })
  }, []);

  return (
    <UserProvider value = {{user, setUser, unSetUser}}>
      <Router>
        <NavBar />
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/register" element={<Register />} />
          <Route path="/login" element={<Login />} />
          <Route path="/logout" element={<Logout />} />
          <Route path="/products" element={<Products />} />
          <Route path="/products/:productId" element={<ProductDetails />} />
          <Route path="/adminDashboard" element={<AdminDashboard />} />
          <Route path="/my-orders" element={<MyOrders />} />
          <Route path="/all-orders" element={<ViewOrders />} />
          <Route path="/product/update/:productId" element={<UpdateProduct />} />
          <Route path="/product/archive/:productId" element={<ArchiveProduct />} />
          <Route path="/order/:productId" element={<OrderProduct />} />
          <Route path="*" element={<PageNotFound/>} />
        </Routes>
      </Router>
    </UserProvider>
  );
}

export default App;
